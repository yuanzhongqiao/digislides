<?php

session_start();

require 'headers.php';

if (!empty($_FILES['blob']) && !empty($_POST['presentation'])) {
	$ancienfichier = $_POST['ancienfichier'];
	$presentation = $_POST['presentation'];
	$extension = pathinfo($_FILES['blob']['name'], PATHINFO_EXTENSION);
	if (!file_exists('../fichiers/' . $presentation)) {
		mkdir('../fichiers/' . $presentation, 0775, true);
	}
	$nom = hash('md5', $_FILES['blob']['tmp_name']) . time() . '.' . $extension;
	$chemin = '../fichiers/' . $presentation . '/' . $nom;
	if (move_uploaded_file($_FILES['blob']['tmp_name'], $chemin)) {
		if ($ancienfichier !== '') {
			if (file_exists('../fichiers/' . $presentation . '/' . $ancienfichier)) {
				unlink('../fichiers/' . $presentation . '/' . $ancienfichier);
			}
		}
		echo $nom;
	} else {
		echo 'erreur';
	}
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
