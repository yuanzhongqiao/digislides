### 数码幻灯片


Digislides 是一个用于创建演示文稿的简单应用程序。

它是根据 GNU AGPLv3 许可证发布的。除 Roboto Slab 和 Material Icons 字体（Apache 许可证版本 2.0）外，HKGrotesk、Montserrat、Quicksand、Lato、Open Sans、Source Sans Pro、League Gothic 和 News Cycle 字体（Sil Open Font 许可证 1.1）以及 Ubuntu 字体（Ubuntu字体许可证1.0)

### 准备并安装依赖项

npm install


### 启动开发服务器

npm run dev

### 环境变量（编译前在根目录创建的.env.生产文件）



AUTHORIZED_DOMAINS (* ou liste des domaines autorisés pour les requêtes POST et l'API, séparés par une virgule)
VITE_DOMAIN (hôte de l'application, par exemple https://ladigitale.dev)
VITE_FOLDER (dossier de l'application, par exemple /digislides/)
VITE_PIXABAY_API_KEY (clé API Pixabay)


### 编译和缩小文件
npm run build


### API 需要 PHP 服务器
php -S 127.0.0.1:8000 (pour le développement uniquement)


### Apache 服务器的 .htaccess 配置（生产）



RewriteEngine on
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ index.html




### 演示
https://ladigitale.dev/digislides/



### 支持

开放集体：https://opencollective.com/ladigitale

Liberapay：https://liberapay.com/ladigitale/


### 致谢和学分
Paolo Mauri 的意大利语翻译 ( https://codeberg.org/maupao )